import 'package:flutter/material.dart';
import 'package:flutter_shopp_firebase/views/menu_frame.dart';
import 'package:provider/provider.dart';

import 'core/providers/cart_provider.dart';
import 'core/providers/orders_provider.dart';
import 'core/providers/products_provider.dart';
import 'core/providers/products_provider_drink.dart';
import 'core/providers/products_provider_four.dart';
import 'core/providers/products_provider_one.dart';
import 'core/providers/products_provider_two.dart';
import 'helper/helperfunctions.dart';
import 'utils/view/router.dart';
import 'utils/view/theme_manager.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool userIsLoggedIn;

  @override
  void initState() {
    getLoggedInState();
    super.initState();
  }

  getLoggedInState() async {
    await HelperFunctions.getUserLoggedInSharedPreference().then((value) {
      setState(() {
        userIsLoggedIn = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // We can listen to these providers to any part of application
        // These providers Container not cause rebuild but the listeners
        ChangeNotifierProvider.value(value: ProductsProvider()),
        ChangeNotifierProvider.value(value: ProductsProviderFour()),
        ChangeNotifierProvider.value(value: ProductsProviderOne()),
        ChangeNotifierProvider.value(value: ProductsProviderTwo()),
        ChangeNotifierProvider.value(value: ProductsProviderDrink()),
        ChangeNotifierProvider.value(value: CartProvider()),
        ChangeNotifierProvider.value(value: OrdersProvider()),
//        ChangeNotifierProvider.value(value: ProductModelProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        onGenerateRoute: BaseRouter.generateRoute,
        // initialRoute: productsOverviewRoute,
        title: ThemeManager.appName,
        theme: ThemeManager.lightTheme,
        home: MenuFrame(),
      ),
    );
  }
}
