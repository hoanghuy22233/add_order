
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopp_firebase/shared/badge.dart';
import 'package:flutter_shopp_firebase/widget/products_grid.dart';
import 'package:provider/provider.dart';

import '../core/providers/cart_provider.dart';
import '../utils/view/constant_routs.dart';

enum FilterOptions { Favourites, All }

class HomeFoodFastScreen extends StatefulWidget {
  @override
  _HomeFoodFastScreenState createState() => _HomeFoodFastScreenState();
}

class _HomeFoodFastScreenState extends State<HomeFoodFastScreen> {
  var _showFavouriteOnly = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tất cả Món ăn'),
        actions: [
          Consumer<CartProvider>(
            builder: (_, cartData, ch) {
              return Badge(
                value: cartData.itemsCount.toString(),
                child: ch,
                color: Colors.green,
              );
            },
            child: IconButton(
              icon: Icon(Icons.shopping_cart, color: Colors.grey[600]),
              onPressed: () {
                Navigator.pushNamed(context, cartScreenRoute);
              },
            ),
          ),
          PopupMenuButton(
            onSelected: (FilterOptions option) {
              setState(() {
                if (option == FilterOptions.Favourites) {
                  _showFavouriteOnly = true;
                } else {
                  _showFavouriteOnly = false;
                }
              });
            },
            icon: Icon(Icons.more_vert, color: Colors.grey[600]),
            itemBuilder: (ctx) {
              return [
                PopupMenuItem(
                  child: Text('Only Favourite'),
                  value: FilterOptions.Favourites,
                ),
                PopupMenuItem(
                  child: Text('Show All'),
                  value: FilterOptions.All,
                ),
              ];
            },
          ),
        ],
      ),
      body: ProductsGrid(_showFavouriteOnly),
    );
  }
}
