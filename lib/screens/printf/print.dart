import 'dart:io' show Platform;
import 'dart:typed_data';

import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/material.dart' hide Image;
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_basic/flutter_bluetooth_basic.dart';
import 'package:flutter_shopp_firebase/core/providers/cart_provider.dart';
import 'package:image/image.dart';
import 'package:provider/provider.dart';

class Print extends StatefulWidget {
  // final List<Map<String, dynamic>> data;
  // Print(this.data);
  @override
  _PrintState createState() => _PrintState();
}

class _PrintState extends State<Print> {
  PrinterBluetoothManager _printerManager = PrinterBluetoothManager();
  List<PrinterBluetooth> _devices = [];
  String _devicesMsg;
  BluetoothManager bluetoothManager = BluetoothManager.instance;

  @override
  void initState() {
    if (Platform.isAndroid) {
      bluetoothManager.state.listen((val) {
        print('state = $val');
        if (!mounted) return;
        if (val == 12) {
          print('on');
          initPrinter();
        } else if (val == 10) {
          print('off');
          setState(() => _devicesMsg = 'Bluetooth chưa được kết nối!');
        }
      });
    } else {
      initPrinter();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<CartProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Print'),
      ),
      body: _devices.isEmpty
          ? Center(child: Text(_devicesMsg ?? ''))
          : ListView.builder(
              itemCount: _devices.length,
              itemBuilder: (c, i) {
                return ListTile(
                  leading: Icon(Icons.print),
                  title: Text(_devices[i].name),
                  subtitle: Text(_devices[i].address),
                  onTap: () {
                    _startPrint(_devices[i], context);
                  },
                );
              },
            ),
    );
  }

  void initPrinter() {
    _printerManager.startScan(Duration(seconds: 2));
    _printerManager.scanResults.listen((val) {
      if (!mounted) return;
      setState(() => _devices = val);
      if (_devices.isEmpty)
        setState(() => _devicesMsg = 'Không có thiết bị nào?');
    });
  }

  Future<void> _startPrint(
      PrinterBluetooth printer, BuildContext context) async {
    _printerManager.selectPrinter(printer);
    final result = await _printerManager
        .printTicket(await _ticket(PaperSize.mm80, context));
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        content: Text(result.msg),
      ),
    );
  }

  Future<Ticket> _ticket(PaperSize paper, BuildContext context) async {
    final cart = Provider.of<CartProvider>(context);
    final ticket = Ticket(paper);
    double total = 0;

    // Image assets
    final ByteData data = await rootBundle.load('assets/images/store.png');
    final Uint8List bytes = data.buffer.asUint8List();
    final Image image = decodeImage(bytes);
    ticket.image(image);
    ticket.text(
      'Phiếu hóa đơn',
      styles: PosStyles(
          align: PosAlign.center,
          height: PosTextSize.size2,
          width: PosTextSize.size2),
      linesAfter: 1,
    );

    for (var i = 0; i < cart.cartItems.length; i++) {
      total += cart.cartItems[i].price;
      ticket.text(cart.cartItems[i].title);
      ticket.row([
        PosColumn(
            text: '${cart.cartItems[i].price} x ${cart.cartItems[i].quantity}',
            width: 6),
        PosColumn(text: 'Tổng: ${cart.cartItems[i].price}', width: 6),
      ]);
    }

    ticket.feed(1);
    ticket.row([
      PosColumn(text: 'Tổng:', width: 6, styles: PosStyles(bold: true)),
      PosColumn(text: 'k $total', width: 6, styles: PosStyles(bold: true)),
    ]);
    ticket.feed(2);
    ticket.text('Cảm ơn quý khách!',
        styles: PosStyles(align: PosAlign.center, bold: true));
    ticket.cut();

    return ticket;
  }

  @override
  void dispose() {
    _printerManager.stopScan();
    super.dispose();
  }
}
