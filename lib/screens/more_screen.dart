import 'package:flutter/material.dart';
import 'package:flutter_shopp_firebase/services/auth.dart';
import 'package:flutter_shopp_firebase/views/menu_frame.dart';

import '../utils/view/constant_routs.dart';

class MoreScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tùy chọn'),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text('Tài khoản', style: Theme.of(context).textTheme.title),
            trailing: Icon(Icons.arrow_forward_ios),
            leading: Icon(Icons.account_circle),
            onTap: () {},
          ),
          Divider(),
          ListTile(
            title: Text('Món ăn của tôi',
                style: Theme.of(context).textTheme.title),
            trailing: Icon(Icons.arrow_forward_ios),
            leading: Icon(Icons.shopping_cart),
            onTap: () {
              Navigator.pushNamed(context, myProductsScreenRoute);
            },
          ),
          Divider(),
          ListTile(
            title: Text('Cài đặt', style: Theme.of(context).textTheme.title),
            trailing: Icon(Icons.arrow_forward_ios),
            leading: Icon(Icons.settings),
            onTap: () {},
          ),
          Divider(),
          ListTile(
            title: Text('Giới thiệu', style: Theme.of(context).textTheme.title),
            trailing: Icon(Icons.arrow_forward_ios),
            leading: Icon(Icons.info),
            onTap: () {},
          ),
          Divider(),
          ListTile(
            title: Text('Thoát', style: Theme.of(context).textTheme.title),
            trailing: Icon(Icons.arrow_forward_ios),
            leading: Icon(Icons.exit_to_app),
            onTap: () {
              AuthService().signOut();
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => MenuFrame()));
            },
          ),
        ],
      ),
    );
  }
}
