import 'package:flutter/material.dart';
import 'package:flutter_shopp_firebase/shared/badge.dart';
import 'package:flutter_shopp_firebase/widget/products_list.dart';
import 'package:flutter_shopp_firebase/widget/products_list_drink.dart';
import 'package:flutter_shopp_firebase/widget/products_list_four.dart';
import 'package:flutter_shopp_firebase/widget/products_list_two.dart';
import 'package:provider/provider.dart';

import '../core/providers/cart_provider.dart';
import '../utils/view/constant_routs.dart';

enum FilterOptions { Favourites, All }

class ProductsOverviewScreen extends StatefulWidget {
  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  int currentTab = 0;
  var _showFavouriteOnly = false;
  var _showFavouriteone = false;
  var _showFavouriteTwo = false;
  var _showFavouriteFour = false;
  var _showFavouriteDrink = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Món ăn theo danh mục'),
        actions: [
          Consumer<CartProvider>(
            builder: (_, cartData, ch) {
              return Badge(
                value: cartData.itemsCount.toString(),
                child: ch,
                color: Colors.green,
              );
            },
            child: IconButton(
              icon: Icon(Icons.shopping_cart, color: Colors.grey[600]),
              onPressed: () {
                Navigator.pushNamed(context, cartScreenRoute);
              },
            ),
          ),
          // PopupMenuButton(
          //   onSelected: (FilterOptions option) {
          //     setState(() {
          //       if (option == FilterOptions.Favourites) {
          //         _showFavouriteOnly = true;
          //       } else {
          //         _showFavouriteOnly = false;
          //       }
          //     });
          //   },
          //   icon: Icon(Icons.more_vert, color: Colors.grey[600]),
          //   itemBuilder: (ctx) {
          //     return [
          //       PopupMenuItem(
          //         child: Text('Only Favourite'),
          //         value: FilterOptions.Favourites,
          //       ),
          //       PopupMenuItem(
          //         child: Text('Show All'),
          //         value: FilterOptions.All,
          //       ),
          //     ];
          //   },
          // ),
        ],
      ),
      body: GestureDetector(
        child: Padding(
          padding: EdgeInsets.only(top: 0),
          child: Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        'Bánh tráng',
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.green,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                    ),
                    Container(
                      height: 250.0,
                      child: ProductsListView(_showFavouriteone),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        'Đồ chiên nướng',
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.green,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                    ),
                    Container(
                      height: 250.0,
                      child: ProductsListViewTwo(_showFavouriteTwo),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        'Đồ xiên que',
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.green,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                    ),
                    Container(
                      height: 250.0,
                      child: ProductsListViewFour(_showFavouriteOnly),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        'Đồ uống',
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.green,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                    ),
                    Container(
                      height: 250.0,
                      child: ProductsListViewDrink(_showFavouriteDrink),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
