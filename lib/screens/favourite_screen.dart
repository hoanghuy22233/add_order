
import 'package:flutter/material.dart';
import 'package:flutter_shopp_firebase/shared/badge.dart';
import 'package:flutter_shopp_firebase/widget/products_grid.dart';
import 'package:flutter_shopp_firebase/widget/products_list.dart';
import 'package:flutter_shopp_firebase/widget/products_list_two.dart';
import 'package:provider/provider.dart';

import '../core/providers/cart_provider.dart';
import '../utils/view/constant_routs.dart';

class FavouriteScreen extends StatelessWidget {
  final _showFavouriteOnly = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Món ăn yêu thích'),
        actions: [
          Consumer<CartProvider>(
            builder: (_, cartData, ch) {
              return Badge(
                value: cartData.itemsCount.toString(),
                child: ch,
                color: Colors.green,
              );
            },
            child: IconButton(
              icon: Icon(Icons.shopping_cart, color: Colors.grey[600]),
              onPressed: () {
                Navigator.pushNamed(context, cartScreenRoute);
              },
            ),
          ),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // _showFavourite == true
          //     ? Container(
          //         margin: EdgeInsets.symmetric(horizontal: 20),
          //         child: Text("Bánh tráng"),
          //       )
          //     : Container(),
          ProductsListView != null
              ? Container(
                  height: 250,
                  child: ProductsListView(_showFavouriteOnly),
                )
              : Container(),
          // ProductsListViewTwo != null
          //     ? Container(
          //         margin: EdgeInsets.symmetric(horizontal: 20),
          //         child: Text("Đồ chiên nướng"),
          //       )
          //     : Container(),
          ProductsListViewTwo != null
              ? Container(
                  height: 250,
                  child: ProductsListViewTwo(_showFavouriteOnly),
                )
              : Container(),
          // ProductsGrid != null
          //     ? Container(
          //         margin: EdgeInsets.symmetric(horizontal: 20),
          //         child: Text("Tất cả sản phẩm"),
          //       )
          //     : Container(),
          ProductsGrid != null
              ? Expanded(
                  child: ProductsGrid(_showFavouriteOnly),
                )
              : Container(),
        ],
      ),
    );
  }
}
