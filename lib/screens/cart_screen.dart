import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_shopp_firebase/screens/printf/print.dart';
import 'package:flutter_shopp_firebase/widget/cart_item_widget.dart';
import 'package:intl/intl.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/providers/cart_provider.dart';
import '../core/providers/orders_provider.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  static const String MONEY_UNIT = 'K';
  static final APP_MONEY_FORMAT = new NumberFormat("#,### ${MONEY_UNIT}");

  String _typeSelected = '';
  final LocalAuthentication auth = LocalAuthentication();
  final storage = FlutterSecureStorage();
  Map<DateTime, List<dynamic>> _events;
  List<dynamic> _selectedEvents;
  TextEditingController _eventController;
  SharedPreferences prefs;

  DatabaseReference _ref;
  DatabaseReference _re;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _ref = FirebaseDatabase.instance.reference().child('Contacts');
    _eventController = TextEditingController();
    _events = {};
    _selectedEvents = [];
    DateTime dateTime;
  }

  initPrefs() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      _events = Map<DateTime, List<dynamic>>.from(
          decodeMap(json.decode(prefs.getString("events") ?? "{}")));
    });
  }

  Map<String, dynamic> encodeMap(Map<DateTime, dynamic> map) {
    Map<String, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[key.toString()] = map[key];
    });
    return newMap;
  }

  Map<DateTime, dynamic> decodeMap(Map<String, dynamic> map) {
    Map<DateTime, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[DateTime.parse(key)] = map[key];
    });
    return newMap;
  }

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<CartProvider>(context);
    final orderData = Provider.of<OrdersProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Giỏ hàng của bạn'),
      ),
      body: Column(
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(15),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Tổng',
                    style: TextStyle(fontSize: 20),
                  ),
                  Spacer(),
                  Chip(
                    label: Text(
                      '${APP_MONEY_FORMAT.format(cart.totalPriceAmount)}',
                      style: TextStyle(
                        color: Theme.of(context).primaryTextTheme.title.color,
                      ),
                    ),
                    backgroundColor: Colors.green,
                  ),
                  cart.totalPriceAmount != 0
                      ? FlatButton(
                          child: Text('Đặt hàng ngay'),
                          onPressed: () {
                            // Listen to OrdersProvider
                            Provider.of<OrdersProvider>(context, listen: false)
                                .addOrder(
                              cart.cartItems.values.toList(),
                              cart.totalPriceAmount,
                            );
                            String quantity;
                            String name;
                            cart.clearCart();
                            for (int i = 0;
                                i < orderData.orders.first.products.length;
                                i++) {
                              quantity = orderData
                                  .orders[0].products[i].quantity
                                  .toString();
                              name = orderData.orders[0].products[i].title
                                      .toString() +
                                  "...";
                            }

                            Map<String, String> contact = {
                              'name': name,
                              'node': quantity,
                              'number': DateTime.now().toString(),
                              'type': "Thanh toán: " +
                                  orderData.orders[0].amount.toString(),
                            };
                            _ref.push().set(contact).then((value) {});

                            Alert(
                              context: context,
                              type: AlertType.success,
                              title: "Thông báo",
                              desc: "Đã thêm vào mục đơn hàng",
                              buttons: [
                                DialogButton(
                                  child: Text(
                                    "Đồng ý",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20),
                                  ),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => Print()));
                                  },
                                  width: 120,
                                )
                              ],
                            ).show();
                          },
                          textColor: Colors.green,
                        )
                      : Container()
                ],
              ),
            ),
          ),
          SizedBox(height: 10),
          Expanded(
            child: ListView.builder(
              itemCount: cart.cartItems.length,
              itemBuilder: (ctx, i) => CartItemWidget(
                itemId: cart.cartItems.keys.toList()[i], // CartItem id
                productId: cart.cartItems.values.toList()[i].id,
                price: cart.cartItems.values.toList()[i].price,
                quantity: cart.cartItems.values.toList()[i].quantity,
                title: cart.cartItems.values.toList()[i].title,
                imageUrl: cart.cartItems.values.toList()[i].imageUrl,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
