import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'product_model_provider.dart';

class ProductsProviderOne with ChangeNotifier {
  List<ProductModelProvider> _product = [
    ProductModelProvider(
      id: 'p1',
      title: 'Trộn thập cẩm',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/thap_cam.png',
    ),
    ProductModelProvider(
      id: 'p2',
      title: 'Trộn rong biển',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/rong_bien.png',
    ),
    ProductModelProvider(
      id: 'p3',
      title: 'Cút sốt me - bơ',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/cut_ap_chao.png',
    ),
    ProductModelProvider(
      id: 'p4',
      title: 'Bánh tráng nướng bò',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/banh_trang_nuong.png',
    ),
    ProductModelProvider(
      id: 'p5',
      title: 'Bánh tráng nướng gà xé',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/image_no_name.png',
    ),
    ProductModelProvider(
      id: 'p6',
      title: 'Thập cẩm',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/thap_cam.png',
    ),
  ];

  // getter
  //  List<Product> get products => [..._products];
  List<ProductModelProvider> get product {
    return _product;
  }

  List<ProductModelProvider> get favoriteProducts {
    return _product.where((product) => product.isFavorite).toList();
  }

  Future<void> addProduct(ProductModelProvider product) {
    const String url =
        "https://flutter-shop-7ddca.firebaseio.com/products.json";
    return http
        .post(url,
            body: json.encode({
              'title': product.title,
              'description': product.description,
              'imageUrl': product.imageUrl,
              'price': product.price,
              'isFavorite': product.isFavorite,
            }))
        .then((response) {
      _product.add(product);
      notifyListeners();
    }).catchError((err) {
      // Print Something ...
    });
  }

  void updateProduct(String id, ProductModelProvider product) {
    final productIndex = _product.indexWhere((prod) => prod.id == id);
    if (productIndex >= 0) {
      _product[productIndex] = product;
      notifyListeners();
    }
  }

  void deleteProduct(String id) {
    _product.removeWhere((prod) => prod.id == id);
    notifyListeners();
  }

  ProductModelProvider findProductByIds(String id) {
    return _product.firstWhere((product) => product.id == id);
  }

  void addProductToFavourite(String id) {
    final product = _product.firstWhere((product) => product.id == id);
    if (product.isFavorite == false) {
      product.isFavorite = true;
      notifyListeners();
    }
  }

  void removeProductFromFavourite(String id) {
    final product = _product.firstWhere((product) => product.id == id);
    if (product.isFavorite) {
      product.isFavorite = false;
      notifyListeners();
    }
  }
}
