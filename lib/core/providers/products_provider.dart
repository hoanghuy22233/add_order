import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'product_model_provider.dart';

class ProductsProvider with ChangeNotifier {
  List<ProductModelProvider> _products = [
    ProductModelProvider(
      id: 'p1',
      title: 'Trộn thập cẩm',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/thap_cam.png',
    ),
    ProductModelProvider(
      id: 'p2',
      title: 'Trộn rong biển',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/rong_bien.png',
    ),
    ProductModelProvider(
      id: 'p3',
      title: 'Cút sốt me - bơ',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/cut_ap_chao.png',
    ),
    ProductModelProvider(
      id: 'p4',
      title: 'Bánh tráng nướng bò',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/banh_trang_nuong.png',
    ),
    ProductModelProvider(
      id: 'p5',
      title: 'Bánh tráng nướng gà xé',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/image_no_name.png',
    ),
    ProductModelProvider(
      id: 'p6',
      title: 'Thập cẩm',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/thap_cam.png',
    ),
    ProductModelProvider(
      id: 'p7',
      title: 'Khoai tây chiên bơ',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/khoai.png',
    ),
    ProductModelProvider(
      id: 'p8',
      title: 'Bắp chiên bơ',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/bap_chien_bo.png',
    ),
    ProductModelProvider(
      id: 'p9',
      title: 'Trứng cút nướng phô mai - bơ',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/cut_xao_me.png',
    ),
    ProductModelProvider(
      id: 'p10',
      title: 'Cơm cháy mỡ hành',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/com_chay.png',
    ),
    ProductModelProvider(
      id: 'p11',
      title: 'Nem rán Hà Nội',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/nem.png',
    ),
    ProductModelProvider(
      id: 'p12',
      title: 'Bắp xào',
      description: 'Ngon bổ rẻ!',
      price: 25.000,
      imageUrl: 'assets/images/bap_xao.png',
    ),
    ProductModelProvider(
      id: 'p13',
      title: 'Phô mai que',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/pho_mai_que.png',
    ),
    ProductModelProvider(
      id: 'p14',
      title: 'Sửa tươi chiên giòn',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/sua_tuoi_chien.png',
    ),
    ProductModelProvider(
      id: 'p15',
      title: 'Hotdog phô mai',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/hotdog.png',
    ),
    ProductModelProvider(
      id: 'p16',
      title: 'Hồ lô nướng',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/holo.png',
    ),
    ProductModelProvider(
      id: 'p17',
      title: 'Xúc xích sụn gà',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/xx_sga.png',
    ),
    ProductModelProvider(
      id: 'p18',
      title: 'Gà OK',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/chicken.png',
    ),
    ProductModelProvider(
      id: 'p19',
      title: 'Xúc xích Đức Việt',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/xxduc_viet.png',
    ),
    ProductModelProvider(
      id: 'p20',
      title: 'Cá viên',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/ca_vien.png',
    ),
    ProductModelProvider(
      id: 'p21',
      title: 'Mực ống',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/muc.png',
    ),
    ProductModelProvider(
      id: 'p22',
      title: 'Thanh cua',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/cua.png',
    ),
    ProductModelProvider(
      id: 'p23',
      title: 'Cá hồi',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/ca_hoi.png',
    ),
    ProductModelProvider(
      id: 'p24',
      title: 'Chả bi',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/cha_bi.png',
    ),
    ProductModelProvider(
      id: 'p25',
      title: 'Bào ngư đỏ',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/bao_ngu.png',
    ),
    ProductModelProvider(
      id: 'p26',
      title: 'Mực bao bò',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/muc_bao_bo.png',
    ),
    ProductModelProvider(
      id: 'p27',
      title: 'Shushi',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/sushi.png',
    ),
    ProductModelProvider(
      id: 'p28',
      title: 'Chả Cừu',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/cha_cuu.png',
    ),
    ProductModelProvider(
      id: 'p29',
      title: 'Cảo',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/cao.png',
    ),
    ProductModelProvider(
      id: 'p31',
      title: 'Cút xào me',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/cut_xao_me.png',
    ),
    ProductModelProvider(
      id: 'p32',
      title: 'Cút lộn xào me',
      description: 'Ngon bổ rẻ!',
      price: 50.000,
      imageUrl: 'assets/images/cut_lon_xao_me.png',
    ),
    ProductModelProvider(
      id: 'p30',
      title: 'Sen vàng',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/tra_sen_vang.png',
    ),
    ProductModelProvider(
      id: 'p33',
      title: 'Trà tắc',
      description: 'Ngon bổ rẻ!',
      price: 10.000,
      imageUrl: 'assets/images/tra_tat.png',
    ),
    ProductModelProvider(
      id: 'p34',
      title: 'Nước chanh',
      description: 'Ngon bổ rẻ!',
      price: 15.000,
      imageUrl: 'assets/images/nuoc_chanh.png',
    ),
    ProductModelProvider(
      id: 'p35',
      title: 'Mận ngâm',
      description: 'Ngon bổ rẻ!',
      price: 15.000,
      imageUrl: 'assets/images/man_ngam.png',
    ),
    ProductModelProvider(
      id: 'p36',
      title: 'Nho ngâm',
      description: 'Ngon bổ rẻ!',
      price: 15.000,
      imageUrl: 'assets/images/nho ngam.png',
    ),
    ProductModelProvider(
      id: 'p37',
      title: 'CoCa',
      description: 'Ngon bổ rẻ!',
      price: 12.000,
      imageUrl: 'assets/images/coca.png',
    ),
    ProductModelProvider(
      id: 'p38',
      title: 'Sting',
      description: 'Ngon bổ rẻ!',
      price: 12.000,
      imageUrl: 'assets/images/sting.png',
    ),
    ProductModelProvider(
      id: 'p39',
      title: 'Trà xanh',
      description: 'Ngon bổ rẻ!',
      price: 15.000,
      imageUrl: 'assets/images/tra_xanh_Khong_do.png',
    ),
    ProductModelProvider(
      id: 'p40',
      title: 'C2',
      description: 'Ngon bổ rẻ!',
      price: 12.000,
      imageUrl: 'assets/images/c2.png',
    ),
    ProductModelProvider(
      id: 'p41',
      title: '7 Up',
      description: 'Ngon bổ rẻ!',
      price: 12.000,
      imageUrl: 'assets/images/7up.png',
    ),
    ProductModelProvider(
      id: 'p42',
      title: 'Nước cam',
      description: 'Ngon bổ rẻ!',
      price: 15.000,
      imageUrl: 'assets/images/cam.png',
    ),
    ProductModelProvider(
      id: 'p43',
      title: 'Bia Hà Nội',
      description: 'Ngon bổ rẻ!',
      price: 18.000,
      imageUrl: 'assets/images/bia_HN.png',
    ),
    ProductModelProvider(
      id: 'p44',
      title: 'Bò húc',
      description: 'Ngon bổ rẻ!',
      price: 18.000,
      imageUrl: 'assets/images/bo_huc.png',
    ),
    ProductModelProvider(
      id: 'p45',
      title: 'Trà sâm dứa',
      description: 'Ngon bổ rẻ!',
      price: 3.000,
      imageUrl: 'assets/images/tra_sam_dua.png',
    ),
    ProductModelProvider(
      id: 'p46',
      title: 'Nhân trần',
      description: 'Ngon bổ rẻ!',
      price: 3.000,
      imageUrl: 'assets/images/nhan_tran.png',
    ),
  ];

  // getter
  //  List<Product> get products => [..._products];
  List<ProductModelProvider> get products {
    return _products;
  }

  List<ProductModelProvider> get favoriteProducts {
    return _products.where((product) => product.isFavorite).toList();
  }

  Future<void> addProduct(ProductModelProvider product) {
    const String url =
        "https://flutter-shop-7ddca.firebaseio.com/products.json";
    return http
        .post(url,
            body: json.encode({
              'title': product.title,
              'description': product.description,
              'imageUrl': product.imageUrl,
              'price': product.price,
              'isFavorite': product.isFavorite,
            }))
        .then((response) {
      _products.add(product);
      notifyListeners();
    }).catchError((err) {
      // Print Something ...
    });
  }

  void updateProduct(String id, ProductModelProvider product) {
    final productIndex = _products.indexWhere((prod) => prod.id == id);
    if (productIndex >= 0) {
      _products[productIndex] = product;
      notifyListeners();
    }
  }

  void deleteProduct(String id) {
    _products.removeWhere((prod) => prod.id == id);
    notifyListeners();
  }

  ProductModelProvider findProductById(String id) {
    return _products.firstWhere((product) => product.id == id);
  }

  void addProductToFavourite(String id) {
    final product = _products.firstWhere((product) => product.id == id);
    if (product.isFavorite == false) {
      product.isFavorite = true;
      notifyListeners();
    }
  }

  void removeProductFromFavourite(String id) {
    final product = _products.firstWhere((product) => product.id == id);
    if (product.isFavorite) {
      product.isFavorite = false;
      notifyListeners();
    }
  }
}
