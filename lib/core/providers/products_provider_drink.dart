import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'product_model_provider.dart';

class ProductsProviderDrink with ChangeNotifier {
  List<ProductModelProvider> _product = [
    ProductModelProvider(
      id: 'p30',
      title: 'Sen vàng',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/tra_sen_vang.png',
    ),
    ProductModelProvider(
      id: 'p33',
      title: 'Trà tắc',
      description: 'Ngon bổ rẻ!',
      price: 10.000,
      imageUrl: 'assets/images/tra_tat.png',
    ),
    ProductModelProvider(
      id: 'p34',
      title: 'Nước chanh',
      description: 'Ngon bổ rẻ!',
      price: 15.000,
      imageUrl: 'assets/images/nuoc_chanh.png',
    ),
    ProductModelProvider(
      id: 'p35',
      title: 'Mận ngâm',
      description: 'Ngon bổ rẻ!',
      price: 15.000,
      imageUrl: 'assets/images/man_ngam.png',
    ),
    ProductModelProvider(
      id: 'p36',
      title: 'Nho ngâm',
      description: 'Ngon bổ rẻ!',
      price: 15.000,
      imageUrl: 'assets/images/nho ngam.png',
    ),
    ProductModelProvider(
      id: 'p37',
      title: 'CoCa',
      description: 'Ngon bổ rẻ!',
      price: 12.000,
      imageUrl: 'assets/images/coca.png',
    ),
    ProductModelProvider(
      id: 'p38',
      title: 'Sting',
      description: 'Ngon bổ rẻ!',
      price: 12.000,
      imageUrl: 'assets/images/sting.png',
    ),
    ProductModelProvider(
      id: 'p39',
      title: 'Trà xanh',
      description: 'Ngon bổ rẻ!',
      price: 15.000,
      imageUrl: 'assets/images/tra_xanh_Khong_do.png',
    ),
    ProductModelProvider(
      id: 'p40',
      title: 'C2',
      description: 'Ngon bổ rẻ!',
      price: 12.000,
      imageUrl: 'assets/images/c2.png',
    ),
    ProductModelProvider(
      id: 'p41',
      title: '7 Up',
      description: 'Ngon bổ rẻ!',
      price: 12.000,
      imageUrl: 'assets/images/7up.png',
    ),
    ProductModelProvider(
      id: 'p42',
      title: 'Nước cam',
      description: 'Ngon bổ rẻ!',
      price: 15.000,
      imageUrl: 'assets/images/cam.png',
    ),
    ProductModelProvider(
      id: 'p43',
      title: 'Bia Hà Nội',
      description: 'Ngon bổ rẻ!',
      price: 18.000,
      imageUrl: 'assets/images/bia_HN.png',
    ),
    ProductModelProvider(
      id: 'p44',
      title: 'Bò húc',
      description: 'Ngon bổ rẻ!',
      price: 18.000,
      imageUrl: 'assets/images/bo_huc.png',
    ),
    ProductModelProvider(
      id: 'p45',
      title: 'Trà sâm dứa',
      description: 'Ngon bổ rẻ!',
      price: 3.000,
      imageUrl: 'assets/images/tra_sam_dua.png',
    ),
    ProductModelProvider(
      id: 'p46',
      title: 'Nhân trần',
      description: 'Ngon bổ rẻ!',
      price: 3.000,
      imageUrl: 'assets/images/nhan_tran.png',
    ),
  ];

  // getter
  //  List<Product> get products => [..._products];
  List<ProductModelProvider> get product {
    return _product;
  }

  List<ProductModelProvider> get favoriteProducts {
    return _product.where((product) => product.isFavorite).toList();
  }

  Future<void> addProduct(ProductModelProvider product) {
    const String url =
        "https://flutter-shop-7ddca.firebaseio.com/products.json";
    return http
        .post(url,
            body: json.encode({
              'title': product.title,
              'description': product.description,
              'imageUrl': product.imageUrl,
              'price': product.price,
              'isFavorite': product.isFavorite,
            }))
        .then((response) {
      _product.add(product);
      notifyListeners();
    }).catchError((err) {
      // Print Something ...
    });
  }

  void updateProduct(String id, ProductModelProvider product) {
    final productIndex = _product.indexWhere((prod) => prod.id == id);
    if (productIndex >= 0) {
      _product[productIndex] = product;
      notifyListeners();
    }
  }

  void deleteProduct(String id) {
    _product.removeWhere((prod) => prod.id == id);
    notifyListeners();
  }

  ProductModelProvider findProductById(String id) {
    return _product.firstWhere((product) => product.id == id);
  }

  void addProductToFavourite(String id) {
    final product = _product.firstWhere((product) => product.id == id);
    if (product.isFavorite == false) {
      product.isFavorite = true;
      notifyListeners();
    }
  }

  void removeProductFromFavourite(String id) {
    final product = _product.firstWhere((product) => product.id == id);
    if (product.isFavorite) {
      product.isFavorite = false;
      notifyListeners();
    }
  }
}
