import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'product_model_provider.dart';

class ProductsProviderFour with ChangeNotifier {
  List<ProductModelProvider> _product = [
    ProductModelProvider(
      id: 'p16',
      title: 'Hồ lô nướng',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/holo.png',
    ),
    ProductModelProvider(
      id: 'p17',
      title: 'Xúc xích sụn gà',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/xx_sga.png',
    ),
    ProductModelProvider(
      id: 'p18',
      title: 'Gà OK',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/chicken.png',
    ),
    ProductModelProvider(
      id: 'p19',
      title: 'Xúc xích Đức Việt',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/xxduc_viet.png',
    ),
    ProductModelProvider(
      id: 'p20',
      title: 'Cá viên',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/ca_vien.png',
    ),
    ProductModelProvider(
      id: 'p21',
      title: 'Mực ống',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/muc.png',
    ),
    ProductModelProvider(
      id: 'p22',
      title: 'Thanh cua',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/cua.png',
    ),
    ProductModelProvider(
      id: 'p23',
      title: 'Cá hồi',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/ca_hoi.png',
    ),
    ProductModelProvider(
      id: 'p24',
      title: 'Chả bi',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/cha_bi.png',
    ),
    ProductModelProvider(
      id: 'p25',
      title: 'Bào ngư đỏ',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/bao_ngu.png',
    ),
    ProductModelProvider(
      id: 'p26',
      title: 'Mực bao bò',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/muc_bao_bo.png',
    ),
    ProductModelProvider(
      id: 'p27',
      title: 'Shushi',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/sushi.png',
    ),
    ProductModelProvider(
      id: 'p28',
      title: 'Chả Cừu',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/cha_cuu.png',
    ),
    ProductModelProvider(
      id: 'p29',
      title: 'Cảo',
      description: 'Ngon bổ rẻ!',
      price: 30.000,
      imageUrl: 'assets/images/cao.png',
    ),
  ];

  // getter
  //  List<Product> get products => [..._products];
  List<ProductModelProvider> get product {
    return _product;
  }

  List<ProductModelProvider> get favoriteProducts {
    return _product.where((product) => product.isFavorite).toList();
  }

  Future<void> addProduct(ProductModelProvider product) {
    const String url =
        "https://flutter-shop-7ddca.firebaseio.com/products.json";
    return http
        .post(url,
            body: json.encode({
              'title': product.title,
              'description': product.description,
              'imageUrl': product.imageUrl,
              'price': product.price,
              'isFavorite': product.isFavorite,
            }))
        .then((response) {
      _product.add(product);
      notifyListeners();
    }).catchError((err) {
      // Print Something ...
    });
  }

  void updateProduct(String id, ProductModelProvider product) {
    final productIndex = _product.indexWhere((prod) => prod.id == id);
    if (productIndex >= 0) {
      _product[productIndex] = product;
      notifyListeners();
    }
  }

  void deleteProduct(String id) {
    _product.removeWhere((prod) => prod.id == id);
    notifyListeners();
  }

  ProductModelProvider findProductById(String id) {
    return _product.firstWhere((product) => product.id == id);
  }

  void addProductToFavourite(String id) {
    final product = _product.firstWhere((product) => product.id == id);
    if (product.isFavorite == false) {
      product.isFavorite = true;
      notifyListeners();
    }
  }

  void removeProductFromFavourite(String id) {
    final product = _product.firstWhere((product) => product.id == id);
    if (product.isFavorite) {
      product.isFavorite = false;
      notifyListeners();
    }
  }
}
