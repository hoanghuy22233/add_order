import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'product_model_provider.dart';

class ProductsProviderTwo with ChangeNotifier {
  List<ProductModelProvider> _product = [
    ProductModelProvider(
      id: 'p7',
      title: 'Khoai tây chiên bơ',
      description: 'Ngon bổ rẻ!',
      price: 25,
      imageUrl: 'assets/images/khoai.png',
    ),
    ProductModelProvider(
      id: 'p8',
      title: 'Bắp chiên bơ',
      description: 'Ngon bổ rẻ!',
      price: 25,
      imageUrl: 'assets/images/bap_chien_bo.png',
    ),
    ProductModelProvider(
      id: 'p9',
      title: 'Trứng cút nướng phô mai - bơ',
      description: 'Ngon bổ rẻ!',
      price: 25,
      imageUrl: 'assets/images/cut_xao_me.png',
    ),
    ProductModelProvider(
      id: 'p10',
      title: 'Cơm cháy mỡ hành',
      description: 'Ngon bổ rẻ!',
      price: 30,
      imageUrl: 'assets/images/com_chay.png',
    ),
    ProductModelProvider(
      id: 'p11',
      title: 'Nem rán Hà Nội',
      description: 'Ngon bổ rẻ!',
      price: 30,
      imageUrl: 'assets/images/nem.png',
    ),
    ProductModelProvider(
      id: 'p12',
      title: 'Bắp xào',
      description: 'Ngon bổ rẻ!',
      price: 25,
      imageUrl: 'assets/images/bap_xao.png',
    ),
    ProductModelProvider(
      id: 'p13',
      title: 'Phô mai que',
      description: 'Ngon bổ rẻ!',
      price: 30,
      imageUrl: 'assets/images/pho_mai_que.png',
    ),
    ProductModelProvider(
      id: 'p14',
      title: 'Sửa tươi chiên giòn',
      description: 'Ngon bổ rẻ!',
      price: 30,
      imageUrl: 'assets/images/sua_tuoi_chien.png',
    ),
    ProductModelProvider(
      id: 'p15',
      title: 'Hotdog phô mai',
      description: 'Ngon bổ rẻ!',
      price: 30,
      imageUrl: 'assets/images/hotdog.png',
    ),
    ProductModelProvider(
      id: 'p31',
      title: 'Cút xào me',
      description: 'Ngon bổ rẻ!',
      price: 30,
      imageUrl: 'assets/images/cut_xao_me.png',
    ),
    ProductModelProvider(
      id: 'p32',
      title: 'Cút lộn xào me',
      description: 'Ngon bổ rẻ!',
      price: 50,
      imageUrl: 'assets/images/cut_lon_xao_me.png',
    ),
  ];

  // getter
  //  List<Product> get products => [..._products];
  List<ProductModelProvider> get product {
    return _product;
  }

  List<ProductModelProvider> get favoriteProducts {
    return _product.where((product) => product.isFavorite).toList();
  }

  Future<void> addProduct(ProductModelProvider product) {
    const String url =
        "https://flutter-shop-7ddca.firebaseio.com/products.json";
    return http
        .post(url,
            body: json.encode({
              'title': product.title,
              'description': product.description,
              'imageUrl': product.imageUrl,
              'price': product.price,
              'isFavorite': product.isFavorite,
            }))
        .then((response) {
      _product.add(product);
      notifyListeners();
    }).catchError((err) {
      // Print Something ...
    });
  }

  void updateProduct(String id, ProductModelProvider product) {
    final productIndex = _product.indexWhere((prod) => prod.id == id);
    if (productIndex >= 0) {
      _product[productIndex] = product;
      notifyListeners();
    }
  }

  void deleteProduct(String id) {
    _product.removeWhere((prod) => prod.id == id);
    notifyListeners();
  }

  ProductModelProvider findProductById(String id) {
    return _product.firstWhere((product) => product.id == id);
  }

  void addProductToFavourite(String id) {
    final product = _product.firstWhere((product) => product.id == id);
    if (product.isFavorite == false) {
      product.isFavorite = true;
      notifyListeners();
    }
  }

  void removeProductFromFavourite(String id) {
    final product = _product.firstWhere((product) => product.id == id);
    if (product.isFavorite) {
      product.isFavorite = false;
      notifyListeners();
    }
  }
}
