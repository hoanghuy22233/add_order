import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../core/providers/cart_provider.dart';
import '../core/providers/product_model_provider.dart';
import '../utils/view/constant_routs.dart';

class ProductDetailsWidget extends StatelessWidget {
  final ProductModelProvider product;
  final int index;
  static const String MONEY_UNIT = 'K';
  static final APP_MONEY_FORMAT = new NumberFormat("#,### ${MONEY_UNIT}");

  ProductDetailsWidget({this.product, this.index});

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      SizedBox(height: 15.0),
      Padding(
        padding: EdgeInsets.only(left: 20.0),
        child: Text(product.title,
            style: TextStyle(
                fontFamily: 'Varela',
                fontSize: 42.0,
                fontWeight: FontWeight.bold,
                color: Colors.green)),
      ),
      SizedBox(height: 15.0),
      // -------------------------------- Product Image------------------------------- //
      Hero(
          tag: 'tage$index',
          child: Image.asset(product.imageUrl,
              height: 150.0, width: 100.0, fit: BoxFit.contain)),
      SizedBox(height: 20.0),
      // -------------------------------- Product Price------------------------------- //
      Center(
        child: Text(APP_MONEY_FORMAT.format(product.price),
            style: TextStyle(
                fontFamily: 'Varela',
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
                color: Color(0xFFF17532))),
      ),
      SizedBox(height: 10.0),
      // -------------------------------- Product Name ------------------------------- //
      Center(
        child: Text(product.title,
            style: TextStyle(
                color: Color(0xFF575E67),
                fontFamily: 'Varela',
                fontSize: 24.0)),
      ),
      SizedBox(height: 20.0),
      // -------------------------------- Product Description ------------------------------- //
      Center(
        child: Container(
          width: MediaQuery.of(context).size.width - 50.0,
          child: Text(product.description,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontFamily: 'Varela',
                  fontSize: 16.0,
                  color: Color(0xFFB4B8B9))),
        ),
      ),
      SizedBox(height: 20.0),
      // -------------------------------- Btn Add To Cart------------------------------- //
      Consumer<CartProvider>(
        builder: (BuildContext context, CartProvider cart, Widget ch) {
          return Center(
              child: InkWell(
            child: ch,
            onTap: () {
              cart.addItemToCart(
                  product.id, product.title, product.price, product.imageUrl);
              Navigator.pushNamed(context, cartScreenRoute);
            },
          ));
        },
        child: Container(
            width: MediaQuery.of(context).size.width - 50.0,
            height: 50.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25.0), color: Colors.green),
            child: Center(
                child: Text(
              'Thêm vào giỏ hàng',
              style: TextStyle(
                  fontFamily: 'Varela',
                  fontSize: 14.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ))),
      ),
    ]);
  }
}
