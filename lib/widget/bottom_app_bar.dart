
import 'package:flutter/material.dart';
import 'package:flutter_shopp_firebase/screens/home_food.dart';
import 'package:flutter_shopp_firebase/screens/more_screen.dart';
import 'package:flutter_shopp_firebase/screens/order_screen.dart';
import 'package:flutter_shopp_firebase/screens/products_overview_screen.dart';
import 'package:flutter_shopp_firebase/shared/custom_bottomAppBar.dart';

class SharedBottomAppBar extends StatefulWidget {
  @override
  _SharedBottomAppBarState createState() => _SharedBottomAppBarState();
}

class _SharedBottomAppBarState extends State<SharedBottomAppBar> {
  Widget _lastSelected = ProductsOverviewScreen();

  String _title = 'Home';
  List<Widget> pages = [
    MoreScreen(),
    OrderScreen(),
    // FavouriteScreen(),
    HomeFoodFastScreen(),
    ProductsOverviewScreen(),
  ];
  List<String> titles = [
    'Tùy chọn',
    'Đơn hàng',
    //   'Yêu thích',
    'Tất cả',
    'Trang chủ'
  ];

  void _selectedTab(int index) {
    setState(() {
      print(index);
      _lastSelected = pages[index];
      _title = titles[index];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _lastSelected,
      bottomNavigationBar: CustomBottomAppBar(
        color: Colors.grey,
        selectedColor: Colors.green,
        //  notchedShape: CircularNotchedRectangle(),
        onTabSelected: _selectedTab,
        items: [
          BottomAppBarItem(iconData: Icons.more_horiz, text: 'Tùy chọn'),
          BottomAppBarItem(iconData: Icons.card_travel, text: 'Đơn hàng'),
          //  BottomAppBarItem(iconData: Icons.favorite, text: 'Yêu thích'),
          BottomAppBarItem(iconData: Icons.all_inbox, text: 'Tất cả'),
          BottomAppBarItem(iconData: Icons.home, text: 'Trang chủ'),
        ],
      ),
    );
  }
}
