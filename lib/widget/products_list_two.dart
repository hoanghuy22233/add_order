
import 'package:flutter/material.dart';
import 'package:flutter_shopp_firebase/core/providers/products_provider_two.dart';
import 'package:flutter_shopp_firebase/widget/poroduct_grid_item_no_check.dart';
import 'package:provider/provider.dart';

class ProductsListViewTwo extends StatelessWidget {
  final showFavorites;
  ProductsListViewTwo(this.showFavorites);

  @override
  Widget build(BuildContext context) {
    // listen to ProductsProvider to get all products list
    final productsData = Provider.of<ProductsProviderTwo>(context);
    final products =
        showFavorites ? productsData.favoriteProducts : productsData.product;
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: products.length,
      itemBuilder: (BuildContext context, int index) {
        return ChangeNotifierProvider.value(
          value: products[index],
          child: ProductGridItemNocheck(index),
        );
      },
    );
  }
}
