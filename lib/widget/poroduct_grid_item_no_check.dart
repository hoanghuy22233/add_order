import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../core/providers/cart_provider.dart';
import '../core/providers/product_model_provider.dart';
import '../utils/view/constant_routs.dart';
import '../utils/view/screen_args/product_details_args.dart';

class ProductGridItemNocheck extends StatelessWidget {
  final index;
  static const String MONEY_UNIT = 'K';
  static final APP_MONEY_FORMAT = new NumberFormat("#,### ${MONEY_UNIT}");

  ProductGridItemNocheck(this.index);

  @override
  Widget build(BuildContext context) {
    final product = Provider.of<ProductModelProvider>(context, listen: false);

    return Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 5.0, right: 5.0),
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(
              context,
              productDetailsRoute,
              arguments: ProductDetailsArgs(id: product.id, index: index),
            );
          },
          child: Container(
              // height: MediaQuery.of(context).size.height / 3,
              width: 170.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(5.0, 5.0),
                    color: Colors.grey.withOpacity(0.2),
                    blurRadius: 8.0,
                  ),
                  BoxShadow(
                    offset: Offset(-5.0, -5.0),
                    color: Colors.grey.withOpacity(0.2),
                    blurRadius: 8.0,
                  ),
                ],
              ),
              child: Column(children: [
                Padding(
                    padding: EdgeInsets.all(5.0),
                    // -------------------------------- Favorite icon -------------------------------- //
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Consumer<ProductModelProvider>(
                            builder: (BuildContext context,
                                ProductModelProvider value, _) {
                              return IconButton(
                                icon: value.isFavorite
                                    ? Icon(Icons.favorite, color: Colors.red)
                                    : Icon(Icons.favorite_border,
                                        color: Colors.grey[600]),
                                color: Theme.of(context).accentColor,
                                onPressed: value.changeFavoriteStatus,
                              );
                            },
                          )
                        ])),
                // -------------------------------- Product Image -------------------------------- //
                Container(
                  height: 75.0,
                  width: MediaQuery.of(context).size.width * 0.3,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(product.imageUrl),
                        fit: BoxFit.contain),
                  ),
                ),
                SizedBox(height: 7.0),
                // -------------------------------- Product price and title -------------------------------- //
                Text(APP_MONEY_FORMAT.format(product.price),
                    style: TextStyle(color: Colors.green, fontSize: 14.0)),
                Text(
                  product.title.length <= 15
                      ? product.title
                      : product.title.substring(0, 15) + '...',
                  style: TextStyle(
                      color: Color(0xFF575E67),
                      fontFamily: 'Varela',
                      fontSize: 14.0),
                  maxLines: 1,
                  textAlign: TextAlign.start,
                ),
                Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Container(color: Color(0xFFEBEBEB), height: 1.0)),
                // -------------------------------- footer section (add to cart)------------------------------- //
                Consumer<CartProvider>(
                    builder: (BuildContext context, CartProvider cart, _) {
                  if (cart.checkProductAddedToCart(product.id)) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        InkWell(
                            child: Icon(Icons.remove_circle_outline,
                                color: Colors.green, size: 25.0),
                            onTap: () {
                              cart.decreaseNumberOfProductsInCartItem(
                                  product.id);
                              if (cart.numberOfProductsInSingleItem(
                                      product.id) ==
                                  0) {
                                cart.removeItemFromCart(product.id);
                              }
                            }),
                        Text(
                            cart
                                .numberOfProductsInSingleItem(product.id)
                                .toString(),
                            style: TextStyle(
                                fontFamily: 'Varela',
                                color: Colors.green,
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0)),
                        InkWell(
                          child: Icon(Icons.add_circle_outline,
                              color: Colors.green, size: 25.0),
                          onTap: () {
                            cart.increaseNumberOfProductsInCartItem(product.id);
                          },
                        ),
                      ],
                    );
                  } else {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Icon(Icons.shopping_basket,
                            color: Colors.green, size: 16.0),
                        InkWell(
                          child: Text('Mua ngay',
                              style: TextStyle(
                                  fontFamily: 'Varela',
                                  color: Colors.green,
                                  fontSize: 14.0)),
                          onTap: () {
                            cart.addItemToCart(product.id, product.title,
                                product.price, product.imageUrl);
                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content: Text(
                                    'Đã thêm sản phẩm vào trong giở hàng!'),
                                duration: Duration(seconds: 2),
                                action: SnackBarAction(
                                  label: 'ẩn',
                                  onPressed: () {
                                    cart.removeItemFromCart(product.id);
                                  },
                                ),
                              ),
                            );
                          },
                        )
                      ],
                    );
                  }
                })
              ])),
        ));
  }
}
