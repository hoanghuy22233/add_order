
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../core/models/order_item.dart';

class OrderItemWidget extends StatelessWidget {
  final OrderItem order;
  static const String MONEY_UNIT = 'K';
  static final APP_MONEY_FORMAT = new NumberFormat("#,### ${MONEY_UNIT}");

  OrderItemWidget(this.order);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text('${APP_MONEY_FORMAT.format(order.amount)}'),
            subtitle: Text(
              DateFormat('dd/MM/yyyy hh:mm').format(order.dateTime),
            ),
            trailing: IconButton(
              icon: Icon(Icons.expand_more),
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }
}
