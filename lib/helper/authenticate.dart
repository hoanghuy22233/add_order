import 'package:flutter/material.dart';
import 'package:flutter_shopp_firebase/views/sign_in.dart';
import 'package:flutter_shopp_firebase/views/signup.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  bool showSignIn = true;

  void toggleView() {
    setState(() {
      showSignIn = !showSignIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      return SignIn();
    } else {
      return SignUp();
    }
  }
}
