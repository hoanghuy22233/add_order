import 'package:flutter/material.dart';
import 'package:flutter_shopp_firebase/screens/add_edit_product_screen.dart';
import 'package:flutter_shopp_firebase/screens/cart_screen.dart';
import 'package:flutter_shopp_firebase/screens/home_food.dart';
import 'package:flutter_shopp_firebase/screens/my_products_screen.dart';
import 'package:flutter_shopp_firebase/screens/product_details_screen.dart';
import 'package:flutter_shopp_firebase/screens/products_overview_screen.dart';
import 'package:flutter_shopp_firebase/screens/un_defined_screen.dart';

import '../../utils/view/constant_routs.dart';
import 'screen_args/product_details_args.dart';

class BaseRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case productsOverviewRoute:
        return MaterialPageRoute(builder: (_) => ProductsOverviewScreen());
      case allProductScreenRoute:
        return MaterialPageRoute(builder: (_) => HomeFoodFastScreen());
      case productDetailsRoute:
        ProductDetailsArgs args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => ProductDetailsScreen(
                  productId: args.id,
                  index: args.index,
                ));
      case cartScreenRoute:
      case cartScreenRoute:
        return MaterialPageRoute(builder: (_) => CartScreen());
      case myProductsScreenRoute:
        return MaterialPageRoute(builder: (_) => MyProductsScreen());

      case addEditProductScreenRoute:
        final productId = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => AddEditProductScreen(productId: productId));
      default:
        return MaterialPageRoute(
          builder: (context) => UndefinedScreen(
            name: settings.name,
          ),
        );
    }
  }
}
